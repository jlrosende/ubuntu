
FROM ubuntu:20.04

# [Optional] Uncomment this section to install additional OS packages.
RUN apt update && export DEBIAN_FRONTEND=noninteractive \
    && apt -y install sudo zsh wget git


ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME -s /bin/zsh \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# ********************************************************
# * Anything else you want to do like clean up goes here *
# ********************************************************
# Clean apt cache
RUN rm -rf /var/lib/apt/lists/*

# [Optional] Set the default user. Omit if you want to keep the default as root.
USER $USERNAME

RUN sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

RUN git clone https://github.com/spaceship-prompt/spaceship-prompt.git "/home/$USERNAME/.oh-my-zsh/themes/spaceship-prompt" --depth=1 \
    && ln -s "/home/$USERNAME/.oh-my-zsh/themes/spaceship-prompt/spaceship.zsh-theme" "/home/$USERNAME/.oh-my-zsh/themes/spaceship.zsh-theme"

RUN git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

ADD .zshrc "/home/$USERNAME/.zshrc"

# RUN sed -i '/^plugins=(.*/a <plugins>' /home/$USERNAME/.zshrc
